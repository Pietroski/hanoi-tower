let c = 0;
let n = 3;

function hanoiDisks(n) {
    for (let i = n; i > 0; i--) {
        const disk = document.createElement('div');
        disk.className = "disks";
        disk.id = "disk" + i;
        disk.innerHTML = i;
        disk.nodeValue = i;
        disk.style.width = 5 + i + "vw";
        const startTorwer = document.getElementById("start");
        startTorwer.appendChild(disk);
    }
}

const resultCounter = document.createElement('span');
resultCounter.style.height = "5mvw";
resultCounter.textContent = c;
const resultDestination = document.getElementById('counter');
resultDestination.appendChild(resultCounter);

// const getNumber = document.getElementById('setNumber').value;

const startButton = document.getElementById("submit").addEventListener('click',
    function () {
        const getNumber = document.getElementById('setNumber').value;
        let n = parseInt(getNumber);
        alert("You have chosen: " + getNumber);
        hanoiDisks(n);

        let booleanSelector = true;

        let middleElements = document.getElementById("middle");

        let waitRoom = middleElements.childNodes.length;

        let startRod = document.getElementById("start");
        let offsetRod = document.getElementById("offset");
        let endRod = document.getElementById("end");

        startRod.addEventListener('click', myScript);
        offsetRod.addEventListener('click', myScript);
        endRod.addEventListener('click', myScript);

        let boxesCount = startRod.childElementCount;

        function myScript(e) {

            let ctlec = e.currentTarget.lastElementChild;

            if (booleanSelector == true && waitRoom == 5 && ctlec != null) {
                booleanSelector = !booleanSelector;
                middleElements.insertBefore(ctlec,
                    middleElements.childNodes[0]);
            }

            else if (booleanSelector == false && waitRoom == 6 && (ctlec == null ||
                ctlec.clientWidth > middleElements.childNodes[0].clientWidth)) {
                booleanSelector = !booleanSelector;
                c++;
                resultCounter.textContent = c;
                e.currentTarget.appendChild(middleElements.childNodes[0]);
            }

            if (endRod.childElementCount == boxesCount) {
                alert("You have won!!");
            }

            waitRoom = middleElements.childNodes.length;
        }

    });

const restartButton = document.getElementById("restart").addEventListener('click', function () { location.reload(); });